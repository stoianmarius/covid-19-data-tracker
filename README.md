# COVID-19 Data Tracker

Real-time statistics about coronavirus.

<a href="https://www.buymeacoffee.com/st0ian" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/lato-red.png" alt="Buy Me A Coffee" style="height: 51px !important;width: 217px !important;" ></a>

## Preview

<img src="https://gitlab.com/st0ianm/covid-19-data-tracker/-/raw/master/demo/home.png" alt="Home Screen" width="200"/>
<img src="https://gitlab.com/st0ianm/covid-19-data-tracker/-/raw/master/demo/graphs.png" alt="Home Screen" width="200"/>
<img src="https://gitlab.com/st0ianm/covid-19-data-tracker/-/raw/master/demo/countries.png" alt="Home Screen" width="200"/>

## Development

Steps to start application in debugging mode for Android/iOS.

Note: Make sure you have an emulator running.
```
git clone https://gitlab.com/st0ianm/covid-19-data-tracker.git
cd covid-19-data-tracker
npm install
npm run android / npm run ios
```

## Builds

Coming soon.
