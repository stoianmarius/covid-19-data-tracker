import React, {useState, useEffect} from 'react';
import {ActivityIndicator, FlatList, TextInput} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {createStackNavigator} from '@react-navigation/stack';
import Box from '../components/Base/Box';
import SvgSearch from '../components/Icons/Search';
import CountryCard from '../components/CountryCard';
import Country from './Country';
import i18n from '../i18n';
import {getCountries} from '../utils/api';
import theme from '../utils/theme';
const SearchStack = createStackNavigator();

function SearchStackScreen() {
  return (
    <SearchStack.Navigator headerMode="none">
      <SearchStack.Screen name="Search" component={Search} />
      <SearchStack.Screen name="Country" component={Country} />
    </SearchStack.Navigator>
  );
}

function Search({navigation}) {
  const [listData, setListData] = useState([]);
  const [countriesData, setCountriesData] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [text, setText] = useState('');

  const getData = async () => {
    const data = await getCountries();
    setCountriesData(data);
    setListData(data);
    setRefreshing(false);
  };

  useEffect(() => {
    getData();
  }, []);

  const searchFilter = txt => {
    const newData = countriesData.filter(item => {
      const listItem = `${item.country.toLowerCase()}}`;
      return listItem.indexOf(txt.toLowerCase()) > -1;
    });

    setListData(newData);
  };

  const onRefresh = () => {
    setRefreshing(true);
    getData();
  };

  return (
    <Box as={SafeAreaView} flex={1} bg="bgLight" height="100%" paddingTop={24}>
      <Box
        flexDirection="row"
        justifyContent="center"
        alignItems="center"
        paddingHorizontal={24}
        marginBottom={24}>
        <Box
          flex={1}
          bg="white"
          alignItems="center"
          flexDirection="row"
          borderRadius={12}
          padding={15}>
          <SvgSearch color={theme.colors.light} width={32} />
          <TextInput
            width="90%"
            onChangeText={txt => {
              setText(txt);
              searchFilter(txt);
            }}
            value={text}
            secureTextEntry={false}
            autoCapitalize="words"
            placeholder={i18n.get('searchCountry')}
            paddingLeft={15}
          />
        </Box>
      </Box>

      {listData.length ? (
        <FlatList
          style={{marginTop: 6}}
          data={listData}
          renderItem={i => (
            <CountryCard
              flag={i.item.info.flag}
              country={i.item.country}
              totalCases={i.item.totalCases}
              navigation={navigation}
              last={listData.length === 1}
            />
          )}
          flex={1}
          keyExtractor={(item, index) => index.toString()}
          onEndReachedThreshold={0}
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      ) : (
        <Box flex={1} justifyContent="center" alignItems="center">
          <ActivityIndicator />
        </Box>
      )}
    </Box>
  );
}

export default SearchStackScreen;
