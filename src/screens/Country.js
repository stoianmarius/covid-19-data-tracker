import React, {useEffect, useState} from 'react';
import {ScrollView, Image, ActivityIndicator} from 'react-native';
import Box from '../components/Base/Box';
import SvgBack from '../components/Icons/Back';
import Text from '../components/Base/Text';
import Button from '../components/Base/Button';
import AliveDead from '../components/AliveDead';
import {formatNumber} from '../utils/number';
import {getCountryByName} from '../utils/api';
import theme from '../utils/theme';
import i18n from '../i18n';
import Card from '../components/Card';

export default function Country({route, navigation}) {
  const [countryData, setCountryData] = useState([]);
  const [hasData, setHasData] = useState(false);
  const [loading, setLoading] = useState(true);

  const getData = async () => {
    setLoading(true);
    const data = await getCountryByName(route.params.country);
    setCountryData(data);
    setLoading(false);
    !hasData && setHasData(true);
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <Box flex={1} bg="bgLight" justifyContent="center">
      {!loading ? (
        <ScrollView padding={24} flex={1}>
          <Box
            flex={1}
            flexDirection="row"
            marginBottom={24}
            alignItems="center">
            <Button
              flex={1}
              flexDirection="row"
              alignItems="center"
              onPress={() => navigation.goBack()}>
              <SvgBack color={theme.colors.gray} />
              <Text fontSize={18} color={theme.colors.gray}>
                {i18n.get('back')}
              </Text>
            </Button>
          </Box>
          <Box paddingBottom={24} flex={1}>
            <Box flexDirection="column">
              <Box flexDirection="row" justifyContent="space-between">
                <Box flexDirection="column" alignItems="flex-start" mb={16}>
                  <Text fontSize={14}>
                    {new Date(countryData.lastUpdate).toLocaleString()}
                  </Text>
                  <Text fontSize={23}>
                    {i18n.get('totalCasesIn') + ' ' + countryData.country}
                  </Text>
                  <Text fontSize={50}>
                    {formatNumber(countryData.totalCases)}
                  </Text>
                </Box>
                <Box
                  backgroundColor="white"
                  borderRadius={12}
                  padding={15}
                  height={60}>
                  <Box flex={1} alignItems="center" width={45} height={25}>
                    <Image
                      style={{width: '100%', flex: 1}}
                      source={{
                        uri: countryData.info.flag,
                      }}
                    />
                  </Box>
                </Box>
              </Box>

              <AliveDead
                alive={countryData.totalRecovered}
                dead={countryData.totalDeaths}
                hasData={hasData}
              />

              <Card hasData={hasData}>
                <Box width="50%" justifyContent="space-around">
                  <Text fontSize={14} color="gray">
                    {i18n.get('newCases').toUpperCase()}
                  </Text>
                  <Text fontSize={30} color="dark">
                    {formatNumber(countryData.todayCases)}
                  </Text>
                </Box>
                <Box
                  width="50%"
                  justifyContent="space-around"
                  alignItems="flex-end">
                  <Text fontSize={14} color="gray">
                    {i18n.get('todayDeaths').toUpperCase()}
                  </Text>
                  <Text fontSize={30} color="dark">
                    {formatNumber(countryData.todayDeaths)}
                  </Text>
                </Box>
              </Card>

              <Card hasData={hasData}>
                <Box width="50%" justifyContent="space-around">
                  <Text fontSize={14} color="gray">
                    {i18n.get('activeCases').toUpperCase()}
                  </Text>
                  <Text fontSize={30} color="dark">
                    {formatNumber(countryData.activeCases)}
                  </Text>
                </Box>
                <Box
                  width="50%"
                  justifyContent="space-around"
                  alignItems="flex-end">
                  <Text fontSize={14} color="gray">
                    {i18n.get('seriousOrCritical').toUpperCase()}
                  </Text>
                  <Text fontSize={30} color="dark">
                    {formatNumber(countryData.criticalCases)}
                  </Text>
                </Box>
              </Card>
            </Box>
          </Box>
        </ScrollView>
      ) : (
        <ActivityIndicator />
      )}
    </Box>
  );
}
