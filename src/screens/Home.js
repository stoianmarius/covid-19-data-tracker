import {createStackNavigator} from '@react-navigation/stack';
import React, {useState, useEffect} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {
  ScrollView,
  RefreshControl,
  Linking,
  TouchableOpacity,
} from 'react-native';
import BuyMeACoffee from '../components/Icons/BuyMeACoffee';
import Box from '../components/Base/Box';
import Text from '../components/Base/Text';
import AliveDead from '../components/AliveDead';
import Card from '../components/Card';
import {getWorldwide} from '../utils/api';
import {formatNumber} from '../utils/number';
import i18n from '../i18n';

function HomeScreen() {
  const [worldwideData, setWorldwideData] = useState({
    activeCases: {},
    closedCases: {},
  });
  const [refreshing, setRefreshing] = useState(false);

  const getData = async () => {
    const data = await getWorldwide();
    setWorldwideData(data);
    setRefreshing(false);
  };

  useEffect(() => {
    getData();
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    getData();
  };

  return (
    <Box as={SafeAreaView} bg="bgLight" flex={1}>
      <Box flex={1} paddingHorizontal={24} paddingTop={24}>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <Box flexDirection="row" justifyContent="space-between" mb={16}>
            <Box alignItems="flex-start">
              <Text fontSize={14}>{worldwideData.lastUpdate}</Text>
              <Text fontSize={23}>{i18n.get('appTitle')}</Text>
              <Text fontSize={50}>
                {formatNumber(worldwideData.totalCases)}
              </Text>
            </Box>
            <Box>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL('https://www.buymeacoffee.com/st0ian')
                }>
                <BuyMeACoffee />
              </TouchableOpacity>
            </Box>
          </Box>

          <AliveDead
            alive={worldwideData.totalRecovered}
            dead={worldwideData.totalDeaths}
            hasData={worldwideData.lastUpdate}
          />

          <Card hasData={worldwideData.lastUpdate}>
            <Box flex={2} justifyContent="space-around">
              <Text fontSize={14} color="gray">
                {i18n.get('activeCases').toUpperCase()}
              </Text>
              <Text fontSize={30} color="dark">
                {formatNumber(worldwideData.totalActiveCases)}
              </Text>
            </Box>
            <Box flex={1} flexDirection="column">
              <Box>
                <Text fontSize={20} color="success">
                  {worldwideData.activeCases.mildPercentage + '%'}
                </Text>
                <Text fontSize={14} color="gray">
                  {i18n.get('mildCondition')}
                </Text>
              </Box>
              <Box height={10} />
              <Box>
                <Text fontSize={20} color="danger">
                  {worldwideData.activeCases.criticalPercentage + '%'}
                </Text>
                <Text fontSize={14} color="gray">
                  {i18n.get('criticalCondition')}
                </Text>
              </Box>
            </Box>
          </Card>

          <Card hasData={worldwideData.lastUpdate}>
            <Box flex={2} justifyContent="space-around">
              <Text fontSize={14} color="gray">
                {i18n.get('closedCases').toUpperCase()}
              </Text>
              <Text fontSize={30} color="dark">
                {formatNumber(worldwideData.totalClosedCases)}
              </Text>
            </Box>
            <Box flex={1} flexDirection="column">
              <Box>
                <Text fontSize={20} color="success">
                  {worldwideData.closedCases.recoveredPercentage + '%'}
                </Text>
                <Text fontSize={14} color="gray">
                  {i18n.get('recoveredCondition')}
                </Text>
              </Box>
              <Box height={10} />
              <Box>
                <Text fontSize={20} color="danger">
                  {worldwideData.closedCases.deadPercentage + '%'}
                </Text>
                <Text fontSize={14} color="gray">
                  {i18n.get('deadCondition')}
                </Text>
              </Box>
            </Box>
          </Card>
        </ScrollView>
      </Box>
    </Box>
  );
}

const HomeStack = createStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator headerMode="none">
      <HomeStack.Screen name="Home" component={HomeScreen} />
    </HomeStack.Navigator>
  );
}

export default HomeStackScreen;
