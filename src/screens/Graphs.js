import {createStackNavigator} from '@react-navigation/stack';
import React, {useState, useEffect} from 'react';
import {ScrollView, RefreshControl, Dimensions} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import Box from '../components/Base/Box';
import Text from '../components/Base/Text';
import Graph from '../components/Graph';
import {LineChart, BarChart, PieChart} from 'react-native-chart-kit';
import {formatToCompact} from '../utils/number';
import i18n from '../i18n';
import {getCountries, getGraphs, getWorldwide} from '../utils/api';

const GraphsStack = createStackNavigator();

function GraphsStackScreen() {
  return (
    <GraphsStack.Navigator headerMode="none">
      <GraphsStack.Screen name="Graphs" component={GraphsScreen} />
    </GraphsStack.Navigator>
  );
}

function GraphsScreen() {
  const [graphsData, setGraphsData] = useState({});
  const [hasData, setHasData] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [mostDeadlyGraphData, setMostDeadlyGraphData] = useState({});
  const [recoveryRateGraphData, setRecoveryRateGraphDate] = useState({});

  const buildMostDeadlyCountries = function(countries) {
    // get last 5 most infected countries
    const mostDeadly = countries
      .slice(0, 5)
      .sort((a, b) => (a.totalDeaths > b.totalDeaths ? -1 : 1));
    const labels = [];
    const values = [];
    mostDeadly.forEach(item => {
      labels.push(item.country);
      values.push(item.totalDeaths);
    });
    return {
      labels: labels,
      datasets: [
        {
          data: values,
        },
      ],
    };
  };

  const buildRecoveryRateGraphData = function(data) {
    return [
      {
        name: i18n.get('recovered'),
        count: data.totalRecovered,
        color: '#34C360',
        legendFontColor: '#696969',
        legendFontSize: 14,
      },
      {
        name: i18n.get('deadCondition'),
        count: data.totalDeaths,
        color: '#D81E34',
        legendFontColor: '#696969',
        legendFontSize: 14,
      },
    ];
  };

  const getData = async () => {
    const graphData = await getGraphs();
    const countriesData = await getCountries();
    const worldwideData = await getWorldwide();
    setGraphsData(graphData);
    setMostDeadlyGraphData(buildMostDeadlyCountries(countriesData));
    setRecoveryRateGraphDate(buildRecoveryRateGraphData(worldwideData));
    setRefreshing(false);
    setHasData(true);
  };

  useEffect(() => {
    getData();
  }, []);

  const onRefresh = () => {
    setRefreshing(true);
    getData();
  };

  return (
    <Box as={SafeAreaView} bg="bgLight" flex={1}>
      <Box flex={1} paddingHorizontal={24} paddingTop={24}>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <Box flexDirection="row" marginBottom={24} alignItems="flex-start">
            <Text fontSize={23}>{i18n.get('graphsTitle')}</Text>
          </Box>

          <Graph title={i18n.get('totalCases')} hasData={hasData}>
            {hasData ? (
              <LineChart
                data={{
                  labels: graphsData.totalCases.labels,
                  datasets: [
                    {
                      data: graphsData.totalCases.values,
                      color: () => 'rgba(0, 0, 0, 0.7)',
                    },
                  ],
                }}
                width={Dimensions.get('window').width - 48}
                height={180}
                withDots={false}
                withShadow={false}
                bezier
                formatYLabel={label => formatToCompact(label)}
                chartConfig={{
                  backgroundColor: 'white',
                  backgroundGradientFrom: 'white',
                  backgroundGradientTo: 'white',
                  decimalPlaces: 0,
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                }}
              />
            ) : (
              {}
            )}
          </Graph>

          <Graph title={i18n.get('recoveryRate')} hasData={hasData}>
            {hasData ? (
              <PieChart
                data={recoveryRateGraphData}
                width={Dimensions.get('window').width - 48}
                height={180}
                accessor="count"
                backgroundColor="transparent"
                chartConfig={{
                  backgroundColor: 'white',
                  backgroundGradientFrom: 'white',
                  backgroundGradientTo: 'white',
                  decimalPlaces: 2,
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  labelColor: (opacity = 1) =>
                    `rgba(255, 255, 255, ${opacity})`,
                }}
              />
            ) : (
              {}
            )}
          </Graph>

          <Graph title={i18n.get('mostDeadlyCountries')} hasData={hasData}>
            {hasData ? (
              <BarChart
                data={mostDeadlyGraphData}
                width={Dimensions.get('window').width - 48}
                height={180}
                fromZero={true}
                showBarTops={false}
                withInnerLines={false}
                chartConfig={{
                  backgroundColor: 'white',
                  backgroundGradientFrom: 'white',
                  backgroundGradientTo: 'white',
                  decimalPlaces: 0,
                  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                  labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                  barPercentage: 0.7,
                  barRadius: 2,
                  strokeWidth: 1,
                  formatYLabel: label => formatToCompact(label),
                }}
              />
            ) : (
              {}
            )}
          </Graph>

          <Graph
            title={i18n.get('activeCasesVsTotalRecovered')}
            hasData={hasData}>
            {hasData ? (
              <LineChart
                data={{
                  labels: graphsData.activeCases.labels,
                  datasets: [
                    {
                      data: graphsData.activeCases.values,
                      color: () => 'rgba(255, 96, 96, 0.7)',
                    },
                    {
                      data: graphsData.totalCured.values,
                      color: () => 'rgba(52, 195, 96, 0.7)',
                    },
                  ],
                }}
                width={Dimensions.get('window').width - 48}
                height={180}
                withDots={false}
                withShadow={false}
                bezier
                formatYLabel={label => formatToCompact(label)}
                chartConfig={{
                  backgroundColor: 'white',
                  backgroundGradientFrom: 'white',
                  backgroundGradientTo: 'white',
                  decimalPlaces: 0,
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                }}
              />
            ) : (
              {}
            )}
          </Graph>
        </ScrollView>
      </Box>
    </Box>
  );
}

export default GraphsStackScreen;
