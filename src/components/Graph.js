import React from 'react';
import Box from './Base/Box';
import Text from './Base/Text';
import Placeholder from './Base/Placeholder';
import {Fade, PlaceholderLine} from 'rn-placeholder';

export default function Graph(props) {
  return (
    <Box
      height={250}
      borderRadius={12}
      bg="white"
      marginBottom={16}
      flex={1}
      flexDirection="column"
      alignItems="center"
      paddingVertical={16}
      boxShadow="0 1px 3px rgba(0,0,0,.1)"
      {...props}>
      {props.hasData ? (
        <Box>
          <Text
            fontSize={14}
            alignSelf="flex-start"
            color="gray"
            paddingLeft={12}
            paddingBottom={16}>
            {props.title.toUpperCase()}
          </Text>
          <Box>{props.children}</Box>
        </Box>
      ) : (
        <Placeholder paddingHorizontal={16} Animation={Fade}>
          <PlaceholderLine width={60} />
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine width={30} />
        </Placeholder>
      )}
    </Box>
  );
}
