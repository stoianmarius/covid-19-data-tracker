import React from 'react';
import Box from './Base/Box';
import Button from './Base/Button';
import Text from './Base/Text';
import {Image} from 'react-native';
import {formatNumber} from '../utils/number';

export default React.memo(function CountryCard({
  flag,
  country,
  totalCases,
  navigation,
  last,
}) {
  return (
    <Box
      height={70}
      bg="white"
      paddingHorizontal={20}
      boxShadow="0px 7px 14px #00000008">
      <Button
        flex={1}
        flexDirection="row"
        alignItems="center"
        justifyContent="space-between"
        borderBottomWidth={1}
        borderBottomColor={!last ? '#E1E1E1' : 'transparent'}
        onPress={() => navigation.navigate('Country', {country})}>
        <Box flex={1} mr={3} flexDirection="row" alignItems="center">
          <Box width={35} height={25}>
            <Image
              style={{width: '100%', flex: 1}}
              source={{
                uri: flag,
              }}
            />
          </Box>
          <Text fontSize={20} numberOfLines={1} paddingLeft={8} color="dark">
            {country}
          </Text>
        </Box>

        <Box flexDirection="row" flex={1} justifyContent="flex-end">
          <Box flexDirection="row" alignItems="center">
            <Text mr={10} fontSize={14} color="gray">
              {formatNumber(totalCases)}
            </Text>
          </Box>
        </Box>
      </Button>
    </Box>
  );
});
