import React from 'react';
import Box from './Base/Box';
import Placeholder from './Base/Placeholder';
import {Fade, PlaceholderLine} from 'rn-placeholder';

export default function ContentCard(props) {
  return (
    <Box
      height={150}
      borderRadius={14}
      backgroundColor="white"
      flex={1}
      width="100%"
      flexDirection="row"
      alignItems="center"
      padding={16}
      boxShadow="0 1px 3px rgba(0,0,0,.1)"
      marginBottom={16}
      {...props}>
      {props.hasData ? (
        props.children
      ) : (
        <Placeholder paddingHorizontal={16} Animation={Fade}>
          <PlaceholderLine width={60} />
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine width={30} />
        </Placeholder>
      )}
    </Box>
  );
}
