import {View} from 'react-native';
import styled from 'styled-components/native';
import {
  compose,
  layout,
  flexbox,
  space,
  border,
  shadow,
  color,
  position,
} from 'styled-system';

const Box = styled(View)(
  compose(
    layout,
    flexbox,
    space,
    border,
    shadow,
    color,
    position,
  ),
);

export default Box;
