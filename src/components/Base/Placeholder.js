import {Placeholder as P} from 'rn-placeholder';
import styled from 'styled-components/native';
import {
  compose,
  layout,
  flexbox,
  space,
  border,
  shadow,
  color,
  position,
  background,
  typography,
} from 'styled-system';

const Placeholder = styled(P)(
  compose(
    layout,
    flexbox,
    space,
    border,
    shadow,
    color,
    position,
    background,
    typography,
  ),
);

export default Placeholder;
