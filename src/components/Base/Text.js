import {Text as T} from 'react-native';
import styled from 'styled-components/native';
import {
  compose,
  layout,
  flexbox,
  space,
  border,
  shadow,
  color,
  position,
  background,
  typography,
} from 'styled-system';

const Text = styled(T)(
  compose(
    layout,
    flexbox,
    space,
    border,
    shadow,
    color,
    position,
    background,
    typography,
  ),
);

Text.defaultProps = {
  color: 'dark',
};

export default Text;
