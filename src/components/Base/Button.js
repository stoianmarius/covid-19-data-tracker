import {TouchableOpacity} from 'react-native';
import styled from 'styled-components/native';
import {
  compose,
  layout,
  flexbox,
  space,
  border,
  shadow,
  color,
  position,
  background,
  typography,
} from 'styled-system';

const Button = styled(TouchableOpacity)(
  compose(
    layout,
    flexbox,
    space,
    border,
    shadow,
    color,
    position,
    background,
    typography,
  ),
);

export default Button;
