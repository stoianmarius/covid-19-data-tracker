import React from 'react';
import Box from './Base/Box';
import Button from './Base/Button';
import SvgHome from './Icons/Home';
import SvgGlobe from './Icons/Globe';
import SvgGraph from './Icons/Graph';
import theme from '../utils/theme';

function TabBar({state, descriptors, navigation}) {
  return (
    <Box
      bg="white"
      flexDirection="row"
      borderTopWidth={1}
      borderTopColor="#EBEBEB"
      padding={10}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        return (
          <Button
            key={label}
            flex={1}
            justifyContent="center"
            alignItems="center"
            onPress={onPress}>
            {label === 'Home' && (
              <SvgHome
                color={isFocused ? theme.colors.dark : theme.colors.light}
              />
            )}
            {label === 'Graphs' && (
              <SvgGraph
                color={isFocused ? theme.colors.dark : theme.colors.light}
              />
            )}
            {label === 'World' && (
              <SvgGlobe
                color={isFocused ? theme.colors.dark : theme.colors.light}
              />
            )}
          </Button>
        );
      })}
    </Box>
  );
}

export default TabBar;
