import React from 'react';
import Svg, {G, Line} from 'react-native-svg';

function SvgRecoveredGraph(props) {
  return (
    <Svg viewBox="0 0 142 71" {...props}>
      <G id="Group_2" data-name="Group 2" transform="translate(-56.5 -354.5)">
        <Line
          data-name="Line 3"
          y2="19"
          transform="translate(57.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 4"
          y2="24"
          transform="translate(67.5 400.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 5"
          y2="19"
          transform="translate(77.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 6"
          y2="33"
          transform="translate(87.5 391.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 7"
          y2="19"
          transform="translate(97.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 8"
          y2="24"
          transform="translate(107.5 400.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 9"
          y2="19"
          transform="translate(117.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 10"
          y2="48"
          transform="translate(127.5 376.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 11"
          y2="55"
          transform="translate(137.5 369.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 12"
          y2="63"
          transform="translate(147.5 361.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 13"
          y2="69"
          transform="translate(157.5 355.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 14"
          y2="55"
          transform="translate(167.5 369.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 15"
          y2="19"
          transform="translate(177.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 16"
          y2="24"
          transform="translate(187.5 400.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 17"
          y2="19"
          transform="translate(197.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 3"
          y2="4"
          transform="translate(57.5 420.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 4"
          y2="12"
          transform="translate(67.5 412.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 5"
          y2="4"
          transform="translate(77.5 420.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 6"
          y2="16"
          transform="translate(87.5 408.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 7"
          y2="8"
          transform="translate(97.5 416.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 8"
          y2="4"
          transform="translate(107.5 420.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 9"
          y2="2"
          transform="translate(117.5 422.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 10"
          y2="15"
          transform="translate(127.5 409.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 11"
          y2="27"
          transform="translate(137.5 397.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 12"
          y2="18"
          transform="translate(147.5 406.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 13"
          y2="12"
          transform="translate(157.5 412.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 14"
          y2="24"
          transform="translate(167.5 400.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 15"
          y2="4"
          transform="translate(177.5 420.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 16"
          y2="6"
          transform="translate(187.5 418.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 17"
          y2="3"
          transform="translate(197.5 421.5)"
          fill="none"
          stroke="#fa5252"
          strokeLinecap="round"
          strokeWidth="2"
        />
      </G>
    </Svg>
  );
}

export default SvgRecoveredGraph;
