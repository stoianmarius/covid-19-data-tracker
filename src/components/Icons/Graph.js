import React from 'react';
import Svg, {Path} from 'react-native-svg';

function SvgGraph(props) {
  return (
    <Svg
      width={35}
      height={35}
      fill="none"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="2"
      viewBox="0 0 24 24"
      {...props}>
      <Path d="M13 7h8m0 0v8m0-8l-8 8-4-4-6 6" />
    </Svg>
  );
}

export default SvgGraph;
