import React from 'react';
import Svg, {G, Line} from 'react-native-svg';

function SvgDeadGraph(props) {
  return (
    <Svg viewBox="0 0 142 71" {...props}>
      <G id="Group_2" data-name="Group 2" transform="translate(-56.5 -354.5)">
        <Line
          data-name="Line 3"
          y2="19"
          transform="translate(57.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          data-name="Line 4"
          y2="24"
          transform="translate(67.5 400.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_5"
          data-name="Line 5"
          y2="19"
          transform="translate(77.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_6"
          data-name="Line 6"
          y2="33"
          transform="translate(87.5 391.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_7"
          data-name="Line 7"
          y2="19"
          transform="translate(97.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_8"
          data-name="Line 8"
          y2="24"
          transform="translate(107.5 400.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_9"
          data-name="Line 9"
          y2="19"
          transform="translate(117.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_10"
          data-name="Line 10"
          y2="48"
          transform="translate(127.5 376.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_11"
          data-name="Line 11"
          y2="55"
          transform="translate(137.5 369.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_12"
          data-name="Line 12"
          y2="63"
          transform="translate(147.5 361.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_13"
          data-name="Line 13"
          y2="69"
          transform="translate(157.5 355.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_14"
          data-name="Line 14"
          y2="55"
          transform="translate(167.5 369.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_15"
          data-name="Line 15"
          y2="19"
          transform="translate(177.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_16"
          data-name="Line 16"
          y2="24"
          transform="translate(187.5 400.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_17"
          data-name="Line 17"
          y2="19"
          transform="translate(197.5 405.5)"
          fill="none"
          stroke="#e5e5e5"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_3"
          data-name="Line 3"
          y2="12"
          transform="translate(57.5 412.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_4"
          data-name="Line 4"
          y2="15"
          transform="translate(67.5 409.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_5"
          data-name="Line 5"
          y2="8"
          transform="translate(77.5 416.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_6"
          data-name="Line 6"
          y2="22"
          transform="translate(87.5 402.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_7"
          data-name="Line 7"
          y2="15"
          transform="translate(97.5 409.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_8"
          data-name="Line 8"
          y2="18"
          transform="translate(107.5 406.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_9"
          data-name="Line 9"
          y2="13"
          transform="translate(117.5 411.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_10"
          data-name="Line 10"
          y2="19"
          transform="translate(127.5 405.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_11"
          data-name="Line 11"
          y2="40"
          transform="translate(137.5 384.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_12"
          data-name="Line 12"
          y2="49"
          transform="translate(147.5 375.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_13"
          data-name="Line 13"
          y2="23"
          transform="translate(157.5 401.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_14"
          data-name="Line 14"
          y2="37"
          transform="translate(167.5 387.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_15"
          data-name="Line 15"
          y2="11"
          transform="translate(177.5 413.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_16"
          data-name="Line 16"
          y2="17"
          transform="translate(187.5 407.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
        <Line
          id="Line_17"
          data-name="Line 17"
          y2="6"
          transform="translate(197.5 418.5)"
          fill="none"
          stroke="#34c360"
          strokeLinecap="round"
          strokeWidth="2"
        />
      </G>
    </Svg>
  );
}

export default SvgDeadGraph;
