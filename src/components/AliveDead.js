import React from 'react';
import Box from './Base/Box';
import Text from './Base/Text';
import Placeholder from './Base/Placeholder';
import SvgDeadGraph from '../components/Icons/DeadGraph';
import SvgRecoveredGraph from '../components/Icons/RecoveredGraph';
import {Fade, PlaceholderLine} from 'rn-placeholder';
import {formatNumber} from '../utils/number';
import i18n from '../i18n';

function Card({icon, title, number, numberColor, hasData}) {
  return (
    <Box
      bg="white"
      height={180}
      borderRadius={12}
      flex={1}
      mb={16}
      flexDirection="column"
      alignItems="center"
      paddingVertical={20}
      boxShadow="0 1px 3px rgba(0,0,0,.1)">
      {hasData ? (
        <Box flex={1} justifyContent="flex-start" alignItems="flex-start">
          <Text fontSize={14} color="gray">
            {title.toUpperCase()}
          </Text>
          <Text fontSize={30} color={numberColor}>
            {number}
          </Text>
          <Box paddingTop={8}>{icon}</Box>
        </Box>
      ) : (
        <Placeholder paddingHorizontal={16} Animation={Fade}>
          <PlaceholderLine width={60} />
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine width={30} />
        </Placeholder>
      )}
    </Box>
  );
}

function AliveDead({alive, dead, hasData}) {
  return (
    <Box flexDirection="row" justifyContent="space-between">
      <Card
        icon={<SvgRecoveredGraph width={130} height={69} />}
        number={formatNumber(dead)}
        numberColor="primary"
        title={i18n.get('deaths')}
        hasData={hasData}
      />
      <Box width={16} />
      <Card
        icon={<SvgDeadGraph width={130} height={69} />}
        number={formatNumber(alive)}
        numberColor="success"
        title={i18n.get('recovered')}
        hasData={hasData}
      />
    </Box>
  );
}

export default AliveDead;
