// format a number to be displayed on the screen with comma
export function formatNumber(number) {
  if (!number) {
    return number;
  }
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

// format to compact ex: 100K, 101M
export function formatToCompact(n) {
  if (n < 1e3) {
    return n;
  }
  if (n >= 1e3 && n < 1e6) {
    return +(n / 1e3).toFixed(1) + 'K';
  }
  if (n >= 1e6 && n < 1e9) {
    return +(n / 1e6).toFixed(1) + 'M';
  }
  if (n >= 1e9 && n < 1e12) {
    return +(n / 1e9).toFixed(1) + 'B';
  }
  if (n >= 1e12) {
    return +(n / 1e12).toFixed(1) + 'T';
  }
}
