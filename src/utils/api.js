// api entry point
const API_URL = 'http://api.covid19-data.monster';

// get app settings
export async function getAppSettings() {
  const response = await fetch(`${API_URL}/settings`);
  return await response.json();
}

// worldwide COVID-19 information
export async function getWorldwide() {
  const response = await fetch(`${API_URL}/worldwide`);
  const data = await response.json();

  // convert lastUpdate to a human readable date
  data.lastUpdate = new Date(data.lastUpdate).toLocaleString();

  // additional info needed to be displayed on the screen
  data.totalActiveCases = data.activeCases.mild + data.activeCases.critical;
  data.totalClosedCases = data.closedCases.recovered + data.closedCases.dead;

  data.activeCases.mildPercentage = (
    (100 * data.activeCases.mild) /
    data.totalActiveCases
  ).toFixed(0);
  data.activeCases.criticalPercentage = (
    (100 * data.activeCases.critical) /
    data.totalActiveCases
  ).toFixed(0);

  data.closedCases.recoveredPercentage = (
    (100 * data.closedCases.recovered) /
    data.totalClosedCases
  ).toFixed(0);
  data.closedCases.deadPercentage = (
    (100 * data.closedCases.dead) /
    data.totalClosedCases
  ).toFixed(0);

  return data;
}

// get list of countries affected
export async function getCountries() {
  const response = await fetch(`${API_URL}/countries`);
  return await response.json();
}

// get information about a country by its iso2, iso3, actual name
export async function getCountryByName(name) {
  const response = await fetch(`${API_URL}/countries/${name}`);
  return await response.json();
}

// get graphs data
export async function getGraphs() {
  const response = await fetch(`${API_URL}/graphs`);
  const graphsData = await response.json();

  // convert graphs into the right format and restrict labels length based on app settings coming from the API
  const defaultChartsParts = 6;

  // parse the result
  const result = {};
  ['totalCases', 'activeCases', 'totalCured'].forEach(prop => {
    const graphData = graphsData[prop];
    const values = [];
    const labels = [];
    const tmpLabels = Object.keys(graphData);
    for (let i = defaultChartsParts; i > 0; i--) {
      const partArr = tmpLabels.splice(0, Math.ceil(tmpLabels.length / i));
      const labelValue = i === 1 ? partArr.pop() : partArr[0];
      values.push(graphData[labelValue]);
      labels.push(labelValue);
    }
    result[prop] = {
      values,
      labels,
    };
  });
  return result;
}
