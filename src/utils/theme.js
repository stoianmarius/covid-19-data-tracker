const colors = {
  white: '#FFFFFF',
  primary: '#D81E34',
  bgLight: '#F2F6F8',
  dark: '#000000',
  light: '#ABB1D0',
  gray: '#696969',
  success: '#34C360',
  warning: '#FFE560',
  danger: '#FF6060',
};

const space = [];

export default {
  colors,
  space,
};
