export default {
  // Home
  appTitle: 'COVID-19 Cases',
  deaths: 'Deaths',
  recovered: 'Recovered',
  activeCases: 'Active Cases',
  mildCondition: 'Mild condition',
  criticalCondition: 'Critical',
  closedCases: 'Closed Cases',
  recoveredCondition: 'Recovered',
  deadCondition: 'Dead',

  // Search Countries
  searchCountry: 'Search country',

  // Country View
  totalCasesIn: 'Total Cases in',
  seriousOrCritical: 'Serious / Critical',
  newCases: 'New Cases',
  todayDeaths: 'Today Deaths',
  back: 'Back',

  // Graphs
  graphsTitle: 'COVID-19 Graphs',
  totalCases: 'Total Cases',
  recoveryRate: 'Recovery Rate',
  mostDeadlyCountries: 'Most Deaths',
  activeCasesVsTotalRecovered: 'Active Cases / Total Recovered',
};
