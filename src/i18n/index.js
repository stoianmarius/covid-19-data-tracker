import enUS from './en-US';

class I18N {
  constructor() {
    this.LANG = 'en-US';
  }

  get(key) {
    switch (this.LANG) {
      case 'en-US':
        return enUS[key];
    }
  }
}

export default new I18N();
