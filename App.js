import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {ThemeProvider} from 'styled-components';
import TabBar from './src/components/TabBar';
import theme from './src/utils/theme';

import HomeScreen from './src/screens/Home';
import GraphsScreen from './src/screens/Graphs';
import SearchCountriesScreen from './src/screens/SearchCountries';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <NavigationContainer>
        <Tab.Navigator
          initialRouteName="Home"
          tabBar={props => <TabBar {...props} />}>
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Graphs" component={GraphsScreen} />
          <Tab.Screen name="World" component={SearchCountriesScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    </ThemeProvider>
  );
}
